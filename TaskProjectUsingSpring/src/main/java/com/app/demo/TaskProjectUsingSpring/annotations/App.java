package com.app.demo.TaskProjectUsingSpring.annotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext cont=new AnnotationConfigApplicationContext(PersonConfig.class);
		Person pobj=(Person)cont.getBean("person2");
		System.out.println("Person first name is "+pobj.getFirstname());
		System.out.println("Last name is "+pobj.getLastname());
		
		CreditCard cobj=(CreditCard)cont.getBean("card2");
		System.out.println("Credit Card Number "+cobj.getCard_no());
		System.out.println("Card Type "+cobj.getCardType());
	}

}

