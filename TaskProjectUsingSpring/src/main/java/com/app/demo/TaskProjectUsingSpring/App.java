package com.app.demo.TaskProjectUsingSpring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext cont = new ClassPathXmlApplicationContext("Beans.xml");
		Task t1 = (Task) cont.getBean("taskobj");
		System.out.println("Task title is " + t1.getTaskTitle());
		Task t2 = (Task) cont.getBean("taskobj1");
		System.out.println("Task title for new objectis " + t2.getTaskTitle());
	}
}
