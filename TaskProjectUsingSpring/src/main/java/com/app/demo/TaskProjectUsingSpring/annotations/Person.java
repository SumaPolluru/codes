package com.app.demo.TaskProjectUsingSpring.annotations;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Person {
	private String firstname;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	private String lastname;
	private String address;
	public List<CreditCard> creditlist;

	public List<CreditCard> getCreditlist() {
		return creditlist;
	}

	public void setCreditlist(List<CreditCard> creditlist) {
		this.creditlist = creditlist;
	}

}
