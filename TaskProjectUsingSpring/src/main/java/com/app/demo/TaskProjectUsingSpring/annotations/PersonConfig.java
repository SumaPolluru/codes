package com.app.demo.TaskProjectUsingSpring.annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonConfig {
	@Bean(name="person1")
	public Person getPerson1()
	{
		Person p1=new Person();
		p1.setFirstname("Bondita");
		p1.setLastname("Mishra");
		p1.setAddress("Kolkatta");
		return p1;
	}
	@Bean(name="person2")
	public Person getPerson2()
	{
		Person p1=new Person();
		p1.setFirstname("Daksh");
		p1.setLastname("Sharma");
		p1.setAddress("Delhi");
		return p1;
	}

	
	@Bean(name="card1")
	public CreditCard getCard1()
	{
		CreditCard c1=new CreditCard();
		c1.setCard_no(78092456);
		c1.setCardType("VISA");	
		return c1;
	}
	@Bean(name="card2")
	public CreditCard getCard2()
	{
		CreditCard c2=new CreditCard();
		c2.setCard_no(78056);
		c2.setCardType("MASTER");	
		return c2;
	}

}
