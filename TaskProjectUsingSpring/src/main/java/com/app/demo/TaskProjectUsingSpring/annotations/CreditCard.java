package com.app.demo.TaskProjectUsingSpring.annotations;

import org.springframework.stereotype.Component;

@Component
public class CreditCard {
	private int card_no;

	public int getCard_no() {
		return card_no;
	}

	public void setCard_no(int card_no) {
		this.card_no = card_no;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	private String cardType;

}
