package com.app.demo.TaskProjectUsingSpring;

public class Task {
	private int taskid;

	public int getTaskid() {
		return taskid;
	}

	public void setTaskid(int taskid) {
		this.taskid = taskid;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Task(int taskid, String taskTitle, int duration, String assignTo) {
		super();
		this.taskid = taskid;
		this.taskTitle = taskTitle;
		this.duration = duration;
		this.assignTo = assignTo;
	}

	public Task() {

	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	private String taskTitle;
	private int duration;
	private String assignTo;

}