<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
      <h2>Student Information</h2>
      <form:form method = "POST" action = "addProduct" modelAttribute="prddata">
         <table>
            <tr>
               <td><form:label path = "productId">ProductID</form:label></td>
               <td><form:input path = "productId" /></td>
            </tr>
            <tr>
               <td><form:label path = "productName">Productname</form:label></td>
               <td><form:input path = "productName" /></td>
            </tr>
            <tr>
               <td><form:label path = "price">Price</form:label></td>
               <td><form:input path = "price" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
         </table>  
      </form:form>
   </body>
   
</html>