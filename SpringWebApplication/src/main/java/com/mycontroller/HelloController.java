
package com.mycontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mycontroller.pojo.Product;

@Controller
public class HelloController {
	// http://localhost:8080/SpringWeb/
	@RequestMapping("/")
	public String getPage() {
		return "welcomepage";// it will search for welcome.jsp under WEb-inf/jsp folder
	}

	@RequestMapping("/ProductData")
	public ModelAndView getForm() {
		return new ModelAndView("productform", "prddata", new Product());
	}

	@RequestMapping("/addProduct")
	public ModelAndView display(@ModelAttribute("prddata") Product p1) {
		// System.out.println("Product name entered by user is "+p1.getProductname());
		// System.out.println("Price entered by user is "+p1.getPrice());
		// return "success";
		return new ModelAndView("success", "productdata", p1);
	}
	/*
	 * First parameter is name of jsp page. second parameter is the name of model
	 * object Third parameter is the object which u want to sent
	 */

}
