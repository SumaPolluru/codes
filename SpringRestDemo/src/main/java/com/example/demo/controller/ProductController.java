package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.ProductDAO;
import com.example.demo.model.Product;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ApiResponse;

@RestController

public class ProductController {
	@Autowired
	ProductDAO pdao;

	@ApiOperation(value = "Product List", notes = "This method will retreive list from database", nickname = "getProducts")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
			@ApiResponse(code = 404, message = "Product not found"),
			@ApiResponse(code = 200, message = "Successful retrieval", response = Product.class, responseContainer = "List") })

	@GetMapping("/Retreive")
	public List<Product> getProductList() {

		List<Product> prdlist = pdao.getData();
		return prdlist;
	}

	@GetMapping("/DisplayForm")
	public ModelAndView getForm() {
		System.out.println("Hey,Displaying");
		return new ModelAndView("ProductForm", "prddata", new Product());
	}

	@GetMapping("/RetreiveByID/{productId}")
	public Product getProductData(@PathVariable("productId") int prdid) {
		Product pdata = pdao.getById(prdid);
		return pdata;
	}

	@PostMapping("/saveData")
	public Product saveData(@ModelAttribute("prddata") Product p1) {
		pdao.save(p1);
		return p1;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(String error, String logout) {
		System.out.println("Coming here ");
		if (error != null)
			return new ModelAndView("login", "errorM	sg", "Your username and password are invalid.");

		if (logout != null)
			return new ModelAndView("login", "msg", "You have been logged out successfully.");
		return new ModelAndView("login");
	}
	
	 @RequestMapping(value="/logout",method = RequestMethod.GET)
	    public String logout(HttpServletRequest request){
	        HttpSession httpSession = request.getSession();
	        httpSession.invalidate();
	        return "redirect:/login";
	    }


}
