<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label {
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<table>
		<tr>
			<td><a href="logout"><font color=Blue size=5>Logout</font></a></td>
		</tr>
	</table>
	<h1>ADD PRODUCT DETAILS</h1>
	<form:form method="POST" action="saveData" modelAttribute="prddata">
		<table>
			<tr>
				<td><form:label path="productId">
						<font color=Black size=5>Product Id</font>
					</form:label></td>
				<td><form:input path="productId" /></td>
			</tr>
			<tr>
				<td><form:label path="productName">
						<font color=Black size=5>Product Name</font>
					</form:label></td>
				<td><form:input path="productName" /></td>
			</tr>
			<tr>
				<td><form:label path="price">
						<font color=Black size=5>Price</font>
					</form:label></td>
				<td><form:input path="price" /></td>
			</tr>
			<tr>
				<td><form:label path="quantity">
						<font color=Black size=5>Quantity</font>
					</form:label></td>
				<td><form:input path="quantity" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>

</body>

</html>