<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Customer Information</h2>
      <form:form method = "POST" action = "addCustomer" modelAttribute="custdata">
         <table>
            <tr>
               <td><form:label path = "custcode">Customer Code</form:label></td>
               <td><form:input path = "custcode" /></td>
            </tr>
            <tr>
               <td><form:label path = "custname">Customer name</form:label></td>
               <td><form:input path = "custname" /></td>
            </tr>
            <tr>
               <td><form:label path = "amountoutstanding">Amount Outstanding</form:label></td>
               <td><form:input path = "amountoutstanding" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
         </table>  
      </form:form>
</body>
</html>