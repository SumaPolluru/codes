package com.gl.practice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/emp")
public class EmployeeController {

	@RequestMapping("/index")
	public String getFirst() {
		return "EmpWelcome";
	}
}
