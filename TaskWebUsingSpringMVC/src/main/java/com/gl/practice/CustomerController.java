package com.gl.practice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.practice.model.Customer;


@Controller
@RequestMapping("/cust")
public class CustomerController {

	@RequestMapping("/index")
	public String getPage() {
		return "CustomerPage";
	}
	
	@RequestMapping("/CustomerData")
	public ModelAndView getForm() {
		return new ModelAndView("Customerform", "custdata", new Customer());
	}

	@RequestMapping("/addCustomer")
	public ModelAndView display(@ModelAttribute("custdata") Customer c1) {
		return new ModelAndView("RetrieveCustomer", "customerdata", c1);
	}
}