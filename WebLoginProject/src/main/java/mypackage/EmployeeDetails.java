package mypackage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mypackage.dataconnect.DataConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Servlet implementation class EmployeeDetails
 */
	@WebServlet("/EmployeeDetails")
	public class EmployeeDetails extends HttpServlet {
		
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			Connection con=DataConnect.getConnection();
			int employeeid=Integer.parseInt(request.getParameter("empid"));
			String employeename=request.getParameter("empname");
			double empsalary=Double.parseDouble(request.getParameter("tsalary"));
			try
			{
			PreparedStatement stat=
					con.prepareStatement("insert into employees values(?,?,?)");
			stat.setInt(1,employeeid);
			stat.setString(2, employeename);
			stat.setDouble(3, empsalary);
			int result=stat.executeUpdate();
			if(result>0)
			{
				
				System.out.println("Data inserted");
				
			}
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}
			
		}

	}
