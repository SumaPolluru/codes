package mypackage;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mypackage.dataconnect.DataConnect;
import pojo.Employees;

import java.sql.Connection;
import java.util.List;
/**
 * Servlet implementation class RetrieveServlet
 */
@WebServlet("/RetrieveServlet")
public class RetrieveServlet extends HttpServlet {
			Connection con1=DataConnect.getConnection();
			PrintWriter out;
			protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				out=response.getWriter();
				HttpSession ses=request.getSession(false);
				List<Employees> emplist=(List<Employees>)ses.getAttribute("elist");
			out.println("<table border=2>");
			out.println("<tr><td>Employee id</td>");
			out.println("<td>Employee name </td>");
			out.println("<td> Salary</td>");
			out.println("</tr>");
			for(Employees e:emplist)
			{
				out.println("<tr>");
				out.println("<td>"+e.getEmpid()+"</td>");
				out.println("<td>"+e.getEmpname()+"</td>");
				out.println("<td>"+e.getSalary()+"</td>");
				out.println("</tr>");
			}
			out.println("</table>");

			}
}
