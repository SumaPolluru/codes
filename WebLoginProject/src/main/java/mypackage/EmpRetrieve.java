package mypackage;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mypackage.dataconnect.DataConnect;
import pojo.Employees;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Servlet implementation class EmpRetrieve
 */
@WebServlet("/EmpRetrieve")
	public class EmpRetrieve extends HttpServlet {
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			Connection con=DataConnect.getConnection();
			int eid=Integer.parseInt(request.getParameter("empid"));
			try
			{
			PreparedStatement stat=con.prepareStatement
					("select * from employees where empid=?");
			stat.setInt(1, eid);
			ResultSet result=stat.executeQuery();
			if(result.next())
			{
				Employees e=new Employees();
				e.setEmpid(result.getInt(1));
				e.setEmpname(result.getString(2));
				e.setSalary(result.getDouble(3));
				HttpSession ses=request.getSession();
				ses.setAttribute("empdetails", e);
				RequestDispatcher dis=getServletContext().getRequestDispatcher
						("/EmpDetails.jsp");
				dis.forward(request, response);
			}
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}		


		}

	}
