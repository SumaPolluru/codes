package mypackage;

import java.io.IOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mypackage.dataconnect.DataConnect;
import pojo.Employees;

import java.sql.Connection;
import java.io.*;
/**
 * Servlet implementation class LoginServlet
 */
/*
 * it will do all work of xml enteries
 * no requirement to make any xml entries
 * it is equivlent to url pattern
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,
	IOException
	{
		Connection con=DataConnect.getConnection();
		PrintWriter out=res.getWriter();
		String log=req.getParameter("tlogin");
		String pass=req.getParameter("tpassword");
		RequestDispatcher dispatch;
		HttpSession ses;
		try
		{
		PreparedStatement stat=con.prepareStatement("select * from user where username=? and password=?");
		stat.setString(1, log);
		stat.setString(2, pass);
		ResultSet result=stat.executeQuery();
		if(result.next())
		{
			List<Employees> emplist=new ArrayList<Employees>();
			ses=req.getSession(true);//it will generate a new session id
			//and will forward it from one page to another.
			ses.setAttribute("loginfo", log);
			stat=con.prepareStatement("select * from Employees");
			ResultSet result1=stat.executeQuery();
			while(result1.next())
			{
				Employees edata=new Employees();
				edata.setEmpid(result1.getInt(1));
				edata.setEmpname(result1.getString(2));
				edata.setSalary(result1.getDouble(3));
				emplist.add(edata);
			}
			ses.setAttribute("elist", emplist);
			dispatch=getServletContext().getRequestDispatcher("/RetrieveServlet");
			dispatch.forward(req, res);
		}
		else
		{
			dispatch=getServletContext().getRequestDispatcher("/Login.html");
			dispatch.include(req, res);
			out.println("<font color=red>Invalid Id or password</font>");
		}
	}
		catch(Exception ex)
		{
			System.out.println("Excepiton is "+ex.getMessage());
		}
	}
}
