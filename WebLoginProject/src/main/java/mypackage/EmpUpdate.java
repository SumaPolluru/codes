package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mypackage.dataconnect.DataConnect;
import pojo.Employees;

/**
 * Servlet implementation class EmpUpdate
 */
@WebServlet("/EmpUpdate")
public class EmpUpdate extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection con=DataConnect.getConnection();
		int employeeid=Integer.parseInt(request.getParameter("empid"));
		String employeename=request.getParameter("empname");
		double empsalary=Double.parseDouble(request.getParameter("tsalary"));
		try
		{
		PreparedStatement stat=
				con.prepareStatement("update employees set empname=?,salary=? where empid=?");
		stat.setInt(3,employeeid);
		stat.setString(1, employeename);
		stat.setDouble(2, empsalary);
		int result=stat.executeUpdate();
		PrintWriter out=response.getWriter();
		out.println(result>0 ? "Updated Succesfully":"Failed to update");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}
}